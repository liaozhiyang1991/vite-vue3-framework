export const withInstall = (component, alias) => {
	const comp = component;
	comp.install = app => {
		app.component(comp.name || comp.displayName, component);
		if (alias) {
			app.config.globalProperties[alias] = component;
		}
	};
	return component;
};
export const getAssetsImages = name => {
	return new URL(`/src/assets/image/${name}`, import.meta.url).href;
};
