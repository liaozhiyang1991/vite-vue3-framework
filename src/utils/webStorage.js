import VueStoragePlugin from 'vue-ls';

export const webStorage = VueStoragePlugin.useStorage({
	namespace: 'pro__',
	name: 'ls',
	storage: 'local'
});
