import { defineStore } from 'pinia';
import { webStorage } from '@/utils/webStorage';
import { ACCESS_TOKEN } from '@/mutation-type';
export const useUserStore = defineStore('user', {
	state: () => {
		return { user: null };
	},
	actions: {
		setUser(data) {
			this.user = data;
		},
		logout() {
			return new Promise((resove, reject) => {
				webStorage.ls.remove(ACCESS_TOKEN);
				this.user = null;
				resove();
			});
		}
	}
});
