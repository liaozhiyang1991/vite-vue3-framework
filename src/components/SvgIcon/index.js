import SvgIcon from './index.vue';
import 'virtual:svg-icons-register';

const svgIconPlugin = {
	install(app) {
		// 全局挂载
		app.component('svg-icon', SvgIcon);
	}
};
export default svgIconPlugin;
