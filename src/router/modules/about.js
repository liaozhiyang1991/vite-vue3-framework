import TableLayout from '@/layout/TableLayout.vue';

export default {
	path: '/about',
	name: 'about',
	component: () => TableLayout,
	meta: { title: '关于我们', orderNo: 1 },
	redirect: '/about/index',
	children: [
		{
			path: 'index',
			name: 'about-index',
			component: import('@/views/About/index.vue')
		}
	]
};
