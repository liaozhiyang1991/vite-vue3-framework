import TableLayout from '@/layout/TableLayout.vue';

export default {
	path: '/device',
	name: 'device',
	component: () => TableLayout,
	meta: { title: '设备管理', orderNo: 1 },
	redirect: '/device/index',
	children: [
		{
			path: 'index',
			name: 'device-index',
			component: import('@/views/Device/index.vue')
		}
	]
};
