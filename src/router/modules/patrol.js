import TableLayout from '@/layout/TableLayout.vue';

export default {
	path: '/patrol',
	name: 'patrol',
	component: () => TableLayout,
	meta: { title: '自动巡检', orderNo: 1 },
	redirect: '/patrol/index',
	children: [
		{
			path: 'index',
			name: 'patrol-index',
			component: import('@/views/Patrol/index.vue')
		}
	]
};
