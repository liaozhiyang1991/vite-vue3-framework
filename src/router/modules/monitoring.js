import TableLayout from '@/layout/TableLayout.vue';

export default {
	path: '/monitoring',
	name: 'monitoring',
	component: () => TableLayout,
	meta: { title: '视频监控', orderNo: 1 },
	redirect: '/monitoring/index',
	children: [
		{
			path: 'index',
			name: 'monitoring-index',
			component: import('@/views/Monitoring/index.vue')
		},
		{
			path: 'add',
			name: 'monitoring-add',
			component: import('@/views/Monitoring/add.vue')
		}
	]
};
