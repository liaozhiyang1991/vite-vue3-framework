import TableLayout from '@/layout/TableLayout.vue';

export default {
	path: '/overview',
	name: 'overview',
	component: () => TableLayout,
	meta: { title: '布设点位总览', orderNo: 1 },
	redirect: '/overview/index',
	children: [
		{
			path: 'index',
			name: 'overview-index',
			component: import('@/views/overview/index.vue')
		}
	]
};
