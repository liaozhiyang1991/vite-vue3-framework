import { axios } from '@/utils/request';

export const queryPermissionsByUser = params => {
	return axios({
		url: '/api/sys/permission/getUserPermissionByToken',
		method: 'get',
		params
	});
};

export const getUsers = params => {
	return axios({
		url: '/dev-api/api/getUsers',
		method: 'get',
		params
	});
};
