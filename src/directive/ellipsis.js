import { createApp, h } from 'vue';
import { Tooltip } from 'ant-design-vue';
export default {
	created(el, binding, vnode, prevVnode) {
		// 下面会介绍各个参数的细节
	},
	// 在元素被插入到 DOM 前调用
	beforeMount(el, binding, vnode, prevVnode) {},
	// 在绑定元素的父组件
	// 及他自己的所有子节点都挂载完成后调用
	mounted(el, binding, vnode, prevVnode) {
		let range = document.createRange();
		range.setStart(el, 0);
		range.setEnd(el, el.childNodes.length);
		let domInfo = el.getBoundingClientRect();
		createApp({
			render: () => {
				return h(
					Tooltip,
					{
						props: { title: 'ssss', placement: 'top' }
					},
					[h('span', { innerHTML: binding.value })]
				);
			}
		}).mount(el);
	},
	// 绑定元素的父组件更新前调用
	beforeUpdate(el, binding, vnode, prevVnode) {},
	// 在绑定元素的父组件
	// 及他自己的所有子节点都更新后调用
	updated(el, binding, vnode, prevVnode) {},
	// 绑定元素的父组件卸载前调用
	beforeUnmount(el, binding, vnode, prevVnode) {},
	// 绑定元素的父组件卸载后调用
	unmounted(el, binding, vnode, prevVnode) {}
};
