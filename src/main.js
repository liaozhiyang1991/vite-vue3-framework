import { createApp } from 'vue';
import '@/assets/style/rest.less';
import 'ant-design-vue/dist/antd.less';
import '@/assets/style/style.less';
import App from './App.vue';
import pinia from '@/store';
import router from '@/router';
import svgIcon from '@/components/svgIcon';
import '@/permission';
import { ClickOutSide } from '@/components/ClickOutSide';
import antd from 'ant-design-vue';
const app = createApp(App);
app.use(pinia);
app.use(router);
app.use(svgIcon);
app.use(antd);
app.use(ClickOutSide);
app.mount('#app');
