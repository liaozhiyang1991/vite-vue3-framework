export default {
	success: true,
	code: 200,
	message: '操作成功',
	data: [
		{
			children: [
				{
					id: 'f415e28c8b62659e8d8bf5d889bc755c',
					enterpriseName: '厂区A-F1-ASD001',
					enterpriseFixedCode: '1243040970',
					parentId: '9069922145',
					lngLat: '116.421324,40.074169',
					videoSrc: 'http://210.210.155.35/dr9445/h/h16/02.m3u8',
					address: '新浦路大街6号监控设备',
					status: 1,
					type: 1
				},
				{
					id: 'f415e28c8b62659e8d8bf5d889bc746c',
					enterpriseName: '厂区A-F1-ASD002',
					enterpriseFixedCode: '1243040960',
					parentId: '9069922145',
					lngLat: '116.421319,40.070996',
					videoSrc: 'http://210.210.155.35/dr9445/h/h16/02.m3u8',
					address: '新浦路大街7号监控设备',
					status: 1,
					type: 1
				}
			],
			id: '045b33520d185a724b1a8587b2ff9927',
			enterpriseName: '厂区A',
			enterpriseFixedCode: '9069922145',
			parentId: '8834410923'
		},
		{
			children: [
				{
					id: 'f39e8435de5942053e290201f053437c',
					enterpriseName: '厂区B-F1-ASD001',
					enterpriseFixedCode: '7382224534',
					parentId: '3612256550',
					lngLat: '116.425154,40.074177',
					videoSrc: 'https://player.alicdn.com/video/editor.mp4',
					address: '新浦路大街8号监控设备',
					status: 0,
					type: 2
				}
			],
			id: '1a062e109275ac2216e844d49750e7c0',
			enterpriseName: '厂区B',
			enterpriseFixedCode: '3612256550',
			parentId: '8834410923'
		}
	]
};
