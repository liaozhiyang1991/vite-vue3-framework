export function wrapperEnv(envConf) {
	const result = {};
	for (const envName of Object.keys(envConf)) {
		let value = envConf[envName].replace(/\\n/g, '\n');
		value = value === 'true' ? true : value === 'false' ? false : value;
		if (envName === 'VITE_PORT') {
			value = Number(value);
		} else if (envName === 'VITE_PROXY') {
			value = JSON.parse(value);
		}
		result[envName] = value;
	}
	return result;
}
