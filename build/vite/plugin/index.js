import path from 'path';
import pkg from '../../../package.json';
import { GLOB_CONFIG_FILE_NAME } from '../../constant.js';
/** vue */
import vue from '@vitejs/plugin-vue';
/** 识别jsx语法*/
import vueJsx from '@vitejs/plugin-vue-jsx';
/** 解决setup语法无法添加name，添加name在script标签上 */
import vueSetupExtend from 'vite-plugin-vue-setup-extend';
/** ant-design-vue 按需加载 */
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
/** 本地svg */
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons';
/** mock */
import { viteMockServe } from 'vite-plugin-mock';
/** html */
import { createHtmlPlugin } from 'vite-plugin-html';

function getAppConfigSrc(viteEnv) {
	const { VITE_PUBLIC_PATH } = viteEnv;
	const path = VITE_PUBLIC_PATH.endsWith('/') ? VITE_PUBLIC_PATH : `${VITE_PUBLIC_PATH}/`;
	return `${path || '/'}${GLOB_CONFIG_FILE_NAME}?v=${pkg.version}-${new Date().getTime()}`;
}

export function createVitePlugins(viteEnv, isBuild) {
	const { VITE_GLOB_APP_TITLE } = viteEnv;
	const vitePlugins = [vue(), vueJsx(), vueSetupExtend()];

	vitePlugins.push(
		createSvgIconsPlugin({
			// 指定要缓存的图标文件夹
			iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
			// 指定符号格式
			symbolId: 'icon-[dir]-[name]'
		})
	);
	/** 在index.html中最小化并使用ejs模板语法的插件 */
	vitePlugins.push(
		createHtmlPlugin({
			minify: isBuild,
			inject: {
				// 向ejs模板中注入数据
				data: {
					title: VITE_GLOB_APP_TITLE
				},
				tags: isBuild
					? [
							{
								tag: 'script',
								attrs: {
									type: 'module',
									'data-info': getAppConfigSrc(viteEnv)
								}
							}
					  ]
					: []
			}
		})
	);

	if (!isBuild) {
		vitePlugins.push(
			viteMockServe({
				mockPath: './mock/',
				supportTs: false, // 监听TS文件，这里要注意下js文件的话填false
				localEnabled: !isBuild // 开发环境
			})
		);
	}
	return vitePlugins;
}
