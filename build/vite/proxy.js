export function createProxy(list) {
	const result = {};
	for (const [prefix, target] of list) {
		result[prefix] = {
			target: target,
			changeOrigin: true,
			ws: true,
			rewrite: path => path.replace(new RegExp(`^${prefix}`), '')
		};
	}
	return result;
}
/**
 * {
      '^/jeecg-boot': {
        target: 'http://lcdp.dev.52qicloud.com/api',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api, '')
      }
    }
 */
